/**
 * Client side code.
 */
(function() {
    var app = angular.module("RegApp", []);

    app.controller("RegistrationCtrl", ["$http", RegistrationCtrl]);

    function RegistrationCtrl($http) {
        var self = this; // vm

        self.user = {
            email: "",
            password: "",
            confirmPassword: "",
            gender: "",
            dateOfBirth: "",
            contactNumber:""

        };

        self.displayUser = {
            email: "",
            password: ""
        };

        self.onlyFemale = function (){
            return self.user.gender == "F";
        };

        self.calculateAge = function(birthday) {
            console.log(">>> in calculateAge function");
            //console.log("birthday >>>" + birthday);
            var ageDifMs = Date.now() - birthday.getTime();
            var ageDate = new Date(ageDifMs); // miliseconds from epoch
            //console.log("ageDifMs >> " + ageDifMs);
            //console.log("ageDate >> " + ageDate);
            var minAge = 18;
            self.userAge = Math.abs(ageDate.getUTCFullYear() - 1970);
            self.ageDiff = minAge - self.userAge;
            console.log("userAge >> " + self.userAge);
            console.log("ageDiff >> " + self.ageDiff);
            if (self.userAge >= minAge) {
                return true;
            }else {
                return false;
            }
             
        };

        self.registerUser = function() {
            console.log(self.user.email);
            console.log(self.user.password);
            $http.post("/users", self.user)
                .then(function(result) {
                    console.log(result);
                    self.displayUser.email = result.data.email;
                    self.displayUser.password = result.data.password;
                    
                }).catch(function(e) {
                    console.log(e);
                });
        };
    }

})();